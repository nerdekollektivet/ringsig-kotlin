plugins {
    kotlin("multiplatform")
    `maven-publish`
}

group = "info.bitcoinunlimited.ringsig"
version = "1.0"

val mingwPath = File(System.getenv("MINGW64_DIR") ?: "C:/msys64/mingw64")
val ringsigLib = "libfujisaki_ringsig"

kotlin {
    // Determine host preset.
    val hostOs = System.getProperty("os.name")

    // Create target for the host platform.
    val hostTarget = when {
        hostOs == "Mac OS X" -> macosX64(ringsigLib)
        hostOs == "Linux" -> linuxX64(ringsigLib)
        hostOs.startsWith("Windows") -> mingwX64(ringsigLib)
        else -> throw GradleException("Host OS '$hostOs' is not supported in Kotlin/Native $project.")
    }

    hostTarget.apply {
        compilations["main"].cinterops {
            val libfujisaki_ringsig by creating {
                when (preset) {
                    presets["macosX64"] -> includeDirs.headerFilterOnly("/opt/local/include", "/usr/local/include")
                    presets["linuxX64"] -> includeDirs.headerFilterOnly("/usr/include", "/usr/include/x86_64-linux-gnu")
                    presets["mingwX64"] -> includeDirs.headerFilterOnly(mingwPath.resolve("include"))
                }
            }
        }
    }
}

fun Project.getKtlintConfiguration(): Configuration {
    return configurations.findByName("ktlint") ?: configurations.create("ktlint") {
        val dependency = project.dependencies.create("com.pinterest:ktlint:0.37.2")
        dependencies.add(dependency)
    }
}

/**
 * List of files that are linted.
 */
fun lintedFileList(): List<String> {
    return listOf(
        "build.gradle.kts",
        "src/main/**/*.kt"
    )
}

tasks.register("ktlint", JavaExec::class.java) {
    description = "Check Kotlin code style."
    group = "Verification"
    classpath = getKtlintConfiguration()
    main = "com.pinterest.ktlint.Main"
    args = listOf("--android") + lintedFileList()
}
tasks.register("ktlintFormat", JavaExec::class.java) {
    description = "Fix Kotlin code style deviations."
    group = "formatting"
    classpath = getKtlintConfiguration()
    main = "com.pinterest.ktlint.Main"
    args = listOf("-F", "--android") + lintedFileList()
}
