#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/**
 * Encodes the relationship of two signatures
 */
typedef enum TraceResult {
  /**
   * `Indep` indicates that the two given signatures were constructed with different private
   * keys.
   */
  Indep,
  /**
   * `Linked` indicates that the same private key was used to sign the same message under the
   * same tag. This does not reveal which key performed the double-signature.
   */
  Linked,
  /**
   * The same key was used to sign distinct messages under the same tag. `pubkey_out` reveales
   * that pubkey.
   */
  Revealed,
  InputErrorSig1,
  InputErrorSig2,
  InputErrorTag,
} TraceResult;

/**
 * For data structures that we cannot export. This hides it behind a void
 * pointer.
 */
typedef struct OpaquePtr {
  void *_0;
} OpaquePtr;

struct OpaquePtr generate_keypair(void);

/**
 * Get the public key from a keypair. Returns false on error.
 */
bool get_pubkey(const struct OpaquePtr *keypair, uint8_t (*pubkey_out)[32]);

/**
 * Get the private key from a keypair. Returns false on error.
 */
bool get_privkey(const struct OpaquePtr *keypair, uint8_t (*privkey_out)[64]);

/**
 * Initialize a `Tag` with an issue and no public keys.
 */
struct OpaquePtr init_tag(const uint8_t *issue, uintptr_t n);

/**
 * Add a public key to a tag.
 */
bool tag_add_pubkey(const struct OpaquePtr *tag, const uint8_t *pubkey);

uintptr_t sign(const uint8_t *msg,
               uintptr_t nmsg,
               const struct OpaquePtr *tag,
               const uint8_t *privkey,
               uint8_t (*out_signature)[1024]);

bool verify(const uint8_t *msg,
            uintptr_t nmsg,
            const struct OpaquePtr *tag,
            const uint8_t *sig,
            uintptr_t nsig);

enum TraceResult do_trace(const uint8_t *msg1,
                          uintptr_t nmsg1,
                          const uint8_t *sig1,
                          uintptr_t nsig1,
                          const uint8_t *msg2,
                          uintptr_t nmsg2,
                          const uint8_t *sig2,
                          uintptr_t nsig2,
                          const struct OpaquePtr *tag,
                          uint8_t (*pubkey_out)[32]);

void free_tag(struct OpaquePtr ptr);

void free_keypair(struct OpaquePtr ptr);
