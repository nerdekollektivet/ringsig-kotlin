buildscript {
    val kotlinVersion by extra("1.4.21")
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath ("com.android.tools.build", "gradle", "3.6.3")
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }

}

task<Delete>("distclean") {
    delete(rootProject.buildDir)
    delete(file("$projectDir/contrib/build/").absolutePath)
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

plugins {
    idea
    `kotlin-dsl`
}

// Stop android studio from indexing the contrib folder
idea {
    module {
        excludeDirs.add(file("$projectDir/contrib"))
    }
}
